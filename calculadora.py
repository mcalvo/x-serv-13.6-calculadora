#!/usr/bin/python3

import sys

if len(sys.argv) != 4:
    sys.exit("Usage Error: python3 calculadora.py <function> <operand1> <operand2>")

if sys.argv[1] == "Suma":
    try:
        print(float(sys.argv[2]) + float(sys.argv[3]))
    except ValueError:
        print("ValueError: Operands must be numbers.")
elif sys.argv[1] == "Resta":
    try:
        print(float(sys.argv[2]) - float(sys.argv[3]))
    except ValueError:
        print("ValueError: Operands must be numbers.")
elif sys.argv[1] == "Multi":
    try:
        print(float(sys.argv[2]) * float(sys.argv[3]))
    except ValueError:
        print("ValueError: Operands must be numbers.")
elif sys.argv[1] == "Div":
    try:
        print(float(sys.argv[2]) / float(sys.argv[3]))
    except ZeroDivisionError:
        print("ZeroDivisionError: Cannot divide by zero.")
    except ValueError:
        print("ValueError: Operands must be numbers.")
else:
    sys.exit("Not valid operation.")
